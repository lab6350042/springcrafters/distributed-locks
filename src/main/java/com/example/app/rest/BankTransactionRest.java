package com.example.app.rest;

import com.example.app.domain.BankTransaction;
import com.example.app.repository.BankTransactionRepository;
import com.example.app.service.BankTransactionService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/bank-transaction")
@AllArgsConstructor
public class BankTransactionRest {
    private final BankTransactionRepository bankTransactionRepository;

    private final BankTransactionService bankTransactionService;

    @GetMapping
    public List<BankTransaction> findAll() {
        return bankTransactionRepository.findAll();
    }

    @PostMapping
    public void insert(@RequestBody BankTransaction bankTransaction) {
        bankTransactionService.updateBankTransaction(bankTransaction);
    }
}
