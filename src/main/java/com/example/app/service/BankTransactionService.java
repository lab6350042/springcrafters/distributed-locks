package com.example.app.service;

import com.example.app.domain.BankTransaction;
import com.example.app.repository.BankTransactionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.support.locks.LockRegistry;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class BankTransactionService {

    private final BankTransactionRepository bankTransactionRepository;

    private final LockRegistry lockRegistry;

    public void updateBankTransaction(BankTransaction bankTransaction) {
        var lock = lockRegistry.obtain(String.valueOf(bankTransaction.getId()));

        boolean lockAquired = lock.tryLock();

        if (!lockAquired)
            throw new RuntimeException("The task cannot be executed because the thread is already in use.");

        try {
            // Simulate a slow transaction
            Thread.sleep(10_000);
            bankTransactionRepository.save(bankTransaction);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        } finally {
            lock.unlock();
        }
    }
}

